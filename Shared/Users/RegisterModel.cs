﻿namespace ManageBooks.Users
{
	public class RegisterModel
	{
		public string Username { get; set; }
		public string Password { get; set; }
		public string ConfirmPassword { get; set; }
	}
}
