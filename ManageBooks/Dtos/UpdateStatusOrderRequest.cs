﻿using Shared.Enum;

namespace ManageBooks.Dtos
{
	public class UpdateStatusOrderRequest
	{
		public OrderStatus Status { get; set; }	
	}
}
